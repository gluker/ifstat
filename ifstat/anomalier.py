#!/usr/bin/python2.7

import argparse
import mydbsql
import re
import sys


class anomalier:
    def __init__(self, interface, timestamp = "1970"):
        self.columns = ['rx_bytes', 'rx_packets', 'tx_bytes', 'tx_packets']
        self.tablename = 'interface_lv'
        self.m = mydbsql.MyDbSQL()
        self.interface = interface
        self.timestamp = timestamp
        self.upper_anomalies_start = 3
        self.lower_anomalies_start = 0.3


    def addkeys(self, column):
        keys = eval("[ ['hour'],['interface'],['%s'],['hour','interface']," \
                "['hour', 'interface', '%s'] ]" % (column, column))

        for key in keys:
            keyname = columns = ''

            for column in key:
                keyname += '%s_' % column
                columns += '%s,' % column

            self.m.query("""
ALTER TABLE preprofile ADD KEY %s (%s)""" % (keyname[0:-1], columns[0:-1]))


    def preprofiler(self):
        for column in self.columns:
            print("%s: preprofiling %s..." % (self.interface, column))

            self.m.query("""
DROP TABLE IF EXISTS preprofile""")
            self.m.query("""
CREATE TABLE preprofile AS
SELECT
    interface,
    HOUR(timestamp) AS hour,
    AVG(%s) AS %s
FROM
    %s
WHERE
    timestamp > "%s" AND
    interface = "%s"
GROUP BY
    DAYOFWEEK(timestamp),
    HOUR(timestamp)
""" % (column, column, self.tablename, self.timestamp, self.interface))

            self.addkeys(column)

            self.upper_delta(column)


    def upper_delta(self, column):
        self.m.cur.execute("""
SELECT COUNT(*) FROM %s WHERE interface = "%s"
""" % (self.tablename, self.interface))
        print("total entries = %s" % self.m.cur.fetchone())

        for i in range(10, 31):
            delta = i / 10

            self.m.cur.execute("""
SELECT 
    COUNT(i.%s)
FROM
    preprofile AS p,
    %s AS i
WHERE
    p.interface = i.interface AND
    p.interface = "%s" AND
    p.hour = HOUR(i.timestamp) AND
    i.%s / p.%s > %s
""" % (column, self.tablename, self.interface, column, column, delta))

            print("delta %s: %s" % (delta, self.m.cur.fetchone()[0]))





    #def lower_delta(self, anomalies = 0):


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description =
        "ifstat anomaly researcher, version 0.1", usage="anomalier.py -h")

    parser.add_argument("--interface", required=True, 
        help="interface to calculate anomalies for")

    parser.add_argument("--upper_anomalies", 
        help="number of anomalies upper average values")

    parser.add_argument("--lower_anomalies", 
        help="number of anomalies lower average values")

    parser.add_argument("--start_time", 
        help="timestamp to build anomalies from (example: 2015-01-01 00:00:00")

    parser.add_argument("--end_time", 
        help="timestamp to build anomalies to (example: 2016-10-10 10:10:10")


    args = parser.parse_args()


    for i in range(10, 30):
        print(i/10)

    a = anomalier(args.interface, args.timestamp)

    a.preprofiler()

    #a.upper_delta(args.upper_anomalies)

    #a.lower_delta(args.lower_anomalies)


