import mysql.connector
import sys
from threading import Thread

from configuration import config


class MyDbSQL:
    thread_spawned = False
    debug = False

    def __init__(self, config_db, connection_timeout=10):
        self.config_db = config_db
        self.thread_spawned = False
        try:
            config_db['connection_timeout'] = connection_timeout
            self.cnx = mysql.connector.connect(**config_db)
            self.cur = self.cnx.cursor()
            self.cnx.autocommit = True
        except mysql.connector.Error as err:
            print('Error connecting to MySQL: {}' . format(err))

    def query(self, query, threaded=False):
        if self.debug:
            print('\n' + query + '\n\n')
        self.checked = False
        if threaded:
            if self.thread_spawned:
                print('Error: new thread while previous is active for: %s:%s' %
                      (self.config_db['host'], self.config_db['database']))
                return
            Thread(target=self.check_query, args=(query, 3)).start()
        else:
            self.check_query(query, False)

    def check_query(self, query, *args):
        self.thread_spawned = True
        if hasattr(self, 'cnx') and self.cnx.is_connected:
            self.execute_query(query)
        else:
            if not self.checked:
                self.checked = True
                self.__init__(self.config_db)
                self.check_query(query)
        self.thread_spawned = False

    def execute_query(self, query):
        try:
            self.cur.execute(query)
        except mysql.connector.Error as err:
            print('Query: %s\n' % query)
            print('Error: {}' . format(err))
            sys.exit()

    def dbclose(self):
        self.cnx.commit()
        if self.cur:
            self.cur.close()
        if self.cnx:
            self.cnx.close()

    def __del__(self):
        self.dbclose()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.dbclose()
