from signal import signal, SIGINT
from sys import exit
from os import getcwd, getpid, kill, path
from yaml import load

__version__ = "1.8"


def signal_handler(signal, frame):
        print('Terminating')
        kill(getpid(), 9)


signal(SIGINT, signal_handler)


__location__ = path.realpath(path.join(getcwd(), path.dirname(__file__)))

with open(path.join(__location__, '../config/ifstat.yaml')) as yamlfile:
    config = load(yamlfile)
    if 'interfaces' not in config or config['interfaces'] is None:
        config['interfaces'] = []
