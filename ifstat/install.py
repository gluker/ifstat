#!/usr/bin/env python2.7

import argparse
import socket
import sys
import yaml

from configuration import __location__


def install():
    parser = argparse.ArgumentParser(usage='istat.py install [arguments]')
    parser.add_argument('--hostname', nargs=1)
    parser.add_argument('--sql-user', nargs=1, required=True)
    parser.add_argument('--sql-password', nargs=1, required=True)
    parser.add_argument('--remote-host', nargs=1)
    parser.add_argument('--remote-user', nargs=1)
    parser.add_argument('--remote-password', nargs=1)
    args = parser.parse_args()

    if args.hostname is None:
        args.hostname = [socket.gethostname()]

    for k in args.__dict__:
        if args.__dict__[k] is None:
            args.__dict__[k] = ['']

    with open(__location__ + '/../config/ifstat.yaml.template', 'r') as f:
        content = f.read()

    with open(__location__ + '/../config/ifstat.yaml-', 'w') as f:
        f.write(content.format(
            hostname=args.hostname[0],
            sql_user=args.sql_user[0],
            sql_password=args.sql_password[0],
            remote_host=args.remote_host[0],
            remote_user=args.remote_user[0],
            remote_password=args.remote_password[0]))


if __name__ == '__main__':
    install()
