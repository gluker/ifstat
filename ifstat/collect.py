#!/usr/bin/env python2.7

import datetime
import re
import sys
import time

from configuration import config
from mydbsql import MyDbSQL


class InterfaceStat:
    def __init__(self):
        if 'local_db' in config:
            self.local_db = MyDbSQL(config['local_db'])
        if 'remote_db' in config:
            self.remote_db = MyDbSQL(config['remote_db'])
        self.iface_stat = {}
        self.probe = False

    def check_interface(self):
        if 'local_db' not in config and 'remote_db' not in config:
            print('Error: no sql database configuration')
            sys.exit()

        sqldata = ''

        d = open('/proc/net/dev').read().split('\n')
        timestamp = datetime.datetime.fromtimestamp(time.time()).\
                    strftime('%Y-%m-%d %H:%M:%S')
        for i in range(2, len(d) - 1):
            d[i] = ' ' + d[i]
            x = re.sub(' +', ' ', d[i]).split(' ')
            x[1] = x[1][0:-1]

            if (not len(config['interfaces']) or
               x[1] in config['interfaces']) and x[1] != 'lo':

                if not x[1] in self.iface_stat.keys():
                    self.iface_stat.update({x[1]: {}})

                if self.probe:
                    sqldata += \
                        ('(0,\'%s\',%s,%s,%s,%s,%s,%s,%s,%s,\'%s\'),' %
                         (x[1],
                          int(x[2]) - self.iface_stat[x[1]]['rx_bytes'],
                          int(x[3]) - self.iface_stat[x[1]]['rx_packets'],
                          int(x[4]) - self.iface_stat[x[1]]['rx_errs'],
                          int(x[5]) - self.iface_stat[x[1]]['rx_drop'],
                          int(x[10]) - self.iface_stat[x[1]]['tx_bytes'],
                          int(x[11]) - self.iface_stat[x[1]]['tx_packets'],
                          int(x[12]) - self.iface_stat[x[1]]['tx_errs'],
                          int(x[13]) - self.iface_stat[x[1]]['tx_drop'],
                          timestamp))

                self.iface_stat[x[1]]['rx_bytes'] = int(x[2])
                self.iface_stat[x[1]]['rx_packets'] = int(x[3])
                self.iface_stat[x[1]]['rx_errs'] = int(x[4])
                self.iface_stat[x[1]]['rx_drop'] = int(x[5])
                self.iface_stat[x[1]]['tx_bytes'] = int(x[10])
                self.iface_stat[x[1]]['tx_packets'] = int(x[11])
                self.iface_stat[x[1]]['tx_errs'] = int(x[12])
                self.iface_stat[x[1]]['tx_drop'] = int(x[13])

        if self.probe:
            if 'local_db' in config:
                self.local_db.query('INSERT INTO %s VALUES %s' %
                                    (config['table_db'], sqldata[0:-1]), True)
            if 'remote_db' in config:
                self.remote_db.query('INSERT INTO %s VALUES %s' %
                                     (config['table_db'], sqldata[0:-1]), True)
        else:
            self.probe = True


def collect():
    i = InterfaceStat()

    start = time.time()
    while True:
        try:
            i.check_interface()
        except Exception as e:
            print('UnexpectedError: %s, %s' % (e, sys.exc_info()[0]))
            pass
        time.sleep(config['interval'] -
                   ((time.time() - start) % config['interval']))


if __name__ == '__main__':
    collect()
