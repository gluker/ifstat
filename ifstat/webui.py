#!/usr/bin/env python2.7

import os
import re
import sys
import web

from configuration import config, __location__, __version__
from mydbsql import MyDbSQL


urls = (
    '/stat/(.*)', 'Stat',
    '/(.*)', 'Index'
)

app = web.application(urls, globals())
render = web.template.render(__location__ + '/../webui/template/')
interval = int(config['interval'])


class Stat:
    def __init__(self):
        self.m = MyDbSQL(config['local_db'])

    def GET(self, req=''):
        opts = req.split('/')

        if len(opts[0]) and not re.search(r'[^a-zA-Z0-9]', opts[0]):
            interface = opts[0]
        else:
            return('')

        if len(opts) > 1 and len(opts[1]) and \
                    not re.search(r'[^\- :0-9]', opts[1]):
            time_start = ("'%s'" % opts[1])
        else:
            time_start = 'DATE_SUB(NOW(), INTERVAL 3 HOUR)'

        if len(opts) > 2 and len(opts[2]) and \
                    not re.search(r'[^\- :0-9]', opts[2]):
            time_end = ("'%s'" % opts[2])
        else:
            time_end = 'NOW()'

        overview = (len(opts) > 3 and opts[3] == 'overview')

        self.m.query("""
SELECT * FROM (
    SELECT
        UNIX_TIMESTAMP(CONVERT_TZ(timestamp, '+00:00', @@global.time_zone))
                * 1000 AS stamp,
        rx_bytes / 1024 / 1024 / %s * 8,
        tx_bytes / 1024 / 1024 / %s * 8,
        rx_packets / 1000 / %s,
        tx_packets / 1000 / %s
    FROM
        %s
    WHERE
        interface = '%s' AND
        timestamp > %s AND
        timestamp < %s
    GROUP BY
        UNIX_TIMESTAMP(timestamp) DIV ( /* returning no more than 1000 rows */
                                        SELECT
                                            COUNT(*) * %s / 1000
                                        FROM
                                            %s
                                        WHERE
                                            interface = '%s' AND
                                            timestamp > %s AND
                                            timestamp < %s )
    ORDER BY
        timestamp DESC
) t ORDER BY t.stamp ASC
        """ % (interval, interval, interval, interval, config['table_db'],
               interface, time_start, time_end,
               interval, config['table_db'], interface, time_start, time_end))

        data = self.m.cur.fetchall()

        rx_bytes = tx_bytes = rx_packets = tx_packets = ''
        for row in data:
            rx_bytes += '[%s,%s],' % (row[0], row[1])
            tx_bytes += '[%s,%s],' % (row[0], row[2])
            if not overview:
                rx_packets += '[%s,%s],' % (row[0], row[3])
                tx_packets += '[%s,%s],' % (row[0], row[4])

        return('{ "bytes": [ { "data": [ %s ] %s },'
                            '{ "data": [ %s ] %s } ],'
                '"packets":[ { "data": [ %s ] %s },'
                            '{ "data": [ %s ] %s } ] }\n'
                 % (rx_bytes[0:-1], '' if overview else ',"label":"rx_mbits"',
                    tx_bytes[0:-1], '' if overview else ',"label":"tx_mbits"',
                    rx_packets[0:-1],
                        '' if overview else ',"label":"rx_kpps"',
                    tx_packets[0:-1],
                        '' if overview else ',"label":"tx_kpps"'))


class Index:
    def __init__(self):
        self.m = MyDbSQL(config['local_db'])

    def GET(self, interface=False):
        self.m.cur.execute("""
SELECT DISTINCT(interface) FROM %s ORDER BY interface""" % config['table_db'])
        interfaces = self.m.cur.fetchall()

        if not interface:
            interface = ''.join(interfaces[0])
            raise web.seeother('/%s' % interface)

        self.m.query("""
SELECT
    UNIX_TIMESTAMP(CONVERT_TZ(MIN(timestamp), '+00:00', @@global.time_zone))
        * 1000 AS oldest,
    UNIX_TIMESTAMP(CONVERT_TZ(MAX(timestamp), '+00:00', @@global.time_zone))
        * 1000 AS latest
FROM
    %s
WHERE
    interface = '%s'
""" % (config['table_db'], interface))
        timestamps = self.m.cur.fetchall()

        return render.index(interfaces, interface, interval, timestamps[0])


def webui():
    # web.wsgi.runwsgi = lambda func, addr=None: web.wsgi.runfcgi(func, addr)
    os.chdir(__location__ + '/../webui/')
    try:
        app.run()
    except Exception as e:
        print('UnexpectedError: %s, %s' % (e, sys.exc_info()[0]))


if __name__ == '__main__':
    webui()
