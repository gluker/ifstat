#!/usr/bin/env python2.7

import sys

from configuration import config, __location__
from mydbsql import MyDbSQL


class Profiler:
    def __init__(self):
        self.db = MyDbSQL(config['local_db'], 1000)
        self.hostnames = []
        self.db.cur = self.db.cnx.cursor(buffered=True)

    def list_hostnames(self):
        self.db.query("SHOW TABLES LIKE 'interface_%'")
        tables = self.db.cur.fetchall()
        for i in range(0, len(tables)):
            try:
                _, hostname = tables[i][0].split('_')
                self.hostnames.append(hostname)
            except ValueError as e:
                debug('Error: split by "_" failed for %s' % tables[i][0])

    def preprofile(self, hostname, exclude_anomaly=''):
        self.db.query("DROP TABLE IF EXISTS `preprofile_%s`" % hostname)
        self.db.query(SQL('preprofile_table').format(hostname=hostname))
        self.db.query(SQL('preprofile_query').format(
            hostname=hostname,
            exclude_anomaly=exclude_anomaly.format(hostname=hostname)))

    def profile(self, hostname, exclude_anomaly=''):
        column_avg = ['rx_bytes', 'rx_packets', 'tx_bytes', 'tx_packets']
        column_div = [['rx_bytes', 'rx_packets'], ['tx_bytes', 'tx_packets'],
                      ['tx_bytes', 'rx_bytes'], ['tx_packets', 'rx_packets']]

        self.db.query("DROP TABLE IF EXISTS `profile_%s`" % hostname)
        self.db.query(SQL('profile_table').format(hostname=hostname))
        self.db.query(SQL('profile_prefill').format(hostname=hostname))

        self.db.query("DROP TABLE IF EXISTS `tmp`")
        self.db.query(SQL('temporary_bigint_table'))

        # [rx|tx]_[bytes|packets]__avg_[low|high]
        for column in column_avg:
            self.db.query(SQL('temporary_avg').format(
                hostname=hostname, column=column, less_greater='<',
                exclude_anomaly=exclude_anomaly.format(hostname=hostname)))
            self.db.query(SQL('profile_avg').format(
                hostname=hostname, column=column, low_high='low'))
            self.db.query("TRUNCATE TABLE `tmp`")
            self.db.query(SQL('temporary_avg').format(
                    hostname=hostname, column=column, less_greater='>',
                    exclude_anomaly=exclude_anomaly.format(hostname=hostname)))
            self.db.query(SQL('profile_avg').format(
                hostname=hostname, column=column, low_high='high'))
            self.db.query("TRUNCATE TABLE `tmp`")

        self.db.query("DROP TABLE IF EXISTS `tmp`")
        self.db.query(SQL('temporary_double_table'))

        # rx_tx_drop_errs__avg_[low|high]
        self.db.query(SQL('temporary_errs_drop').format(
            hostname=hostname, less_greater='<',
            exclude_anomaly=exclude_anomaly.format(hostname=hostname)))
        self.db.query(SQL('profile_errs_drop').format(
            hostname=hostname, low_high='low'))
        self.db.query("TRUNCATE TABLE `tmp`")
        self.db.query(SQL('temporary_errs_drop').format(
            hostname=hostname, less_greater='>',
            exclude_anomaly=exclude_anomaly.format(hostname=hostname)))
        self.db.query(SQL('profile_errs_drop').format(
            hostname=hostname, low_high='high'))
        self.db.query("TRUNCATE TABLE `tmp`")

        # [[rx|tx]_bytes|tx_packets]__div__[[rx|tx]_packets|rx_bytes]
        for column in column_div:
            self.db.query(SQL('temporary_div').format(
                hostname=hostname, divided=column[0], divisor=column[1],
                less_greater='<',
                exclude_anomaly=exclude_anomaly.format(hostname=hostname)))
            self.db.query(SQL('profile_div').format(
                hostname=hostname, divided=column[0], divisor=column[1],
                low_high='low'))
            self.db.query("TRUNCATE TABLE `tmp`")
            self.db.query(SQL('temporary_div').format(
                hostname=hostname, divided=column[0], divisor=column[1],
                less_greater='>',
                exclude_anomaly=exclude_anomaly.format(hostname=hostname)))
            self.db.query(SQL('profile_div').format(
                hostname=hostname, divided=column[0], divisor=column[1],
                low_high='high'))
            self.db.query("TRUNCATE TABLE `tmp`")

        self.db.query("OPTIMIZE TABLE `profile_%s`" % hostname)

    def preanomaly(self, hostname):
        self.db.query("DROP TABLE IF EXISTS `preanomaly_%s`" % hostname)
        self.db.query(SQL('preanomaly_table').format(hostname=hostname))
        self.db.query(SQL('preanomaly_query').format(
            hostname=hostname, delta=3))

    def drop_tables(self, hostname):
        self.db.query("DROP TABLE IF EXISTS tmp, preprofile_{hostname}, "
                      "preanomaly_{hostname}".format(hostname=hostname))


def SQL(query):
    try:
        fname = __location__ + '/../config/profile/' + query + '.sql'
        with open(fname, 'r') as f:
            return f.read()
    except IOError as e:
        sys.exit('Error: opening query for %s: %s' % (query, e))


def debug(str):
    if True:
        sys.stdout.write(str).flush()
        sys.stdout.flush()


def profiler(hostname=''):
    import time

    p = Profiler()

    if hostname:
        p.hostnames = [hostname]
    else:
        p.list_hostnames()

    for hostname in p.hostnames:
        start_time = time.time()
        debug('- %s: preprofile avg' % hostname)
        p.preprofile(hostname)
        debug(' low/high')
        p.profile(hostname)
        debug(', anomalies')
        p.preanomaly(hostname)
        debug(', profile avg')
        p.preprofile(hostname, SQL('exclude_anomaly'))
        debug(' low/high')
        p.profile(hostname, SQL('exclude_anomaly'))
        debug(' - %ss\n' % int(time.time() - start_time))
        # p.drop_tables(hostname)


if __name__ == '__main__':
    profiler(sys.argv[1])
