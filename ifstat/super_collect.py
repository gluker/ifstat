#!/usr/bin/env python2.7

import sys
import time

sys.path.append('/var/lib/ifstat')
from configuration import config
from mydbsql import MyDbSQL


class SuperCollect():
    QUERY = """\
INSERT INTO
    super_interface(
        interface,
        rx_bytes, rx_packets, rx_errs, rx_drop,
        tx_bytes, tx_packets, tx_errs, tx_drop,
        timestamp)
SELECT
    super.interface AS super_interface,
    SUM(super.rx_bytes) AS rx_bytes,
    SUM(super.rx_packets) AS rx_packets,
    SUM(super.rx_errs) AS rx_errs,
    SUM(super.rx_drop) AS rx_drop,
    SUM(super.tx_bytes) AS tx_bytes,
    SUM(super.tx_packets) AS tx_packets,
    SUM(super.tx_errs) AS tx_errs,
    SUM(super.tx_drop) AS tx_drop,
    DATE_FORMAT(super.timestamp, '%Y-%m-%d %H:%i') AS super_timestamp
FROM (

{subquery}

) AS super
GROUP BY
    super_interface,
    super_timestamp
"""

    SUBQUERY = """\
SELECT
    '{super_interface}' AS interface,
    rx_bytes, rx_packets, rx_errs, rx_drop,
    tx_bytes, tx_packets, tx_errs, tx_drop,
    timestamp
FROM
    interface_{interface_table}
WHERE
    interface = '{interface_name}' AND
    DATE_FORMAT(timestamp, '%Y-%m-%d %H:%i') > '{super_timestamp}' AND
    DATE_FORMAT(timestamp, '%Y-%m-%d %H:%i') < '{current_timestamp}'
"""

    SUPER_TIMESTAMP = """\
SELECT
    DATE_FORMAT(IFNULL(MAX(timestamp), '1970-01-01 01:01'), '%Y-%m-%d %H:%i')
FROM
    super_interface"""

    CURRENT_TIMESTAMP = """\
SELECT
    DATE_FORMAT(DATE_SUB(NOW(), INTERVAL %s SECOND), '%%Y-%%m-%%d %%H:%%i')
""" % config['interval']

    def __init__(self):
        self.db = MyDbSQL(config['super_db'])

    def collect(self):
        self.db.cur = self.db.cnx.cursor(dictionary=False)

        self.db.query(self.SUPER_TIMESTAMP)
        super_timestamp = str(self.db.cur.fetchone()[0])

        self.db.query(self.CURRENT_TIMESTAMP)
        current_timestamp = str(self.db.cur.fetchone()[0])

        self.db.cur = self.db.cnx.cursor(dictionary=True)

        self.db.query("""
SELECT * FROM super_option ORDER BY super_interface, interface_table""")
        super_option = self.db.cur.fetchall()

        subquery = ''
        for i in range(0, len(super_option)):
            subquery += self.SUBQUERY.format(
                super_interface=super_option[i]['super_interface'],
                interface_table=super_option[i]['interface_table'],
                interface_name=super_option[i]['interface_name'],
                super_timestamp=super_timestamp,
                current_timestamp=current_timestamp)
            if i != len(super_option) - 1:
                subquery += "\nUNION ALL\n\n"

        self.db.query(self.QUERY.format(subquery=subquery))


if __name__ == '__main__':
    s = SuperCollect()

    start = time.time()
    while True:
        try:
            s.collect()
        except Exception as e:
            print('UnexpectedError: %s, %s' % (e, sys.exc_info()[0]))
            pass
        time.sleep(config['interval'] -
                   ((time.time() - start) % config['interval']))
