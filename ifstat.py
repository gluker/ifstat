#!/usr/bin/env python2

"""ifstat cli and web app"""

import os
import sys
import time

from threading import Thread

from ifstat import __version__
from ifstat.configuration import config


def pidfile():
    try:
        if os.path.isfile(config['pidfile']):
            with open(config['pidfile'], 'r') as f:
                old_pid = int(f.read())
            try:
                os.kill(old_pid, 0)
            except OSError:
                os.unlink(config['pidfile'])
            else:
                print('ifstat (pid: %s) is already running, exiting' % old_pid)
                sys.exit()
        new_pid = str(os.getpid())
        file(config['pidfile'], 'w').write(new_pid)
    except Exception as e:
        print('Error: %s, %s' % (e, sys.exc_info()[0]))
        sys.exit()


def daemon():
    from ifstat.collect import collect
    from ifstat.webui import webui

    pidfile()

    threads = []
    threads.append(Thread(target=collect))
    threads.append(Thread(target=webui))
    for thread in threads:
        thread.daemon = True
        thread.start()

    while True:
        time.sleep(1)


def install():
    from ifstat.install import install
    install()


def profiler():
    from ifstat.profiler import profiler
    profiler(sys.argv[1])


def anomaly():
    sys.exit('anomaly')
    from ifstat.anomaly import anomaly
    profile()


def usage(commands):
    help = '''
IFStat is InterFace Statistic tool, version: %s\n
The commands are:\n\n''' % __version__
    for key in commands:
        help += '\t{:<10}-- {}\n' . format(key, commands[key])
    sys.exit(help)


def main():
    commands = {'install': 'prepare ifstat to work',
                'daemon': 'to run both Collect and WebUI thread',
                'profiler': 'to rebuild profiles for traffic',
                'anomaly': 'to show up anomalies for given period'}
    # checks if command-line parameter equals to key in commands and run it
    try:
        sys.argv = sys.argv[1:]
        commands[sys.argv[0]] and globals()[sys.argv[0]]()
    # except (IndexError, KeyError):
    except IndexError:
        usage(commands)


if __name__ == '__main__' or sys.argv[0] == 'uwsgi':
    main()
