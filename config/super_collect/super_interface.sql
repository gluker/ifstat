CREATE TABLE `super_interface` (
  `_interface_stat` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `interface` varchar(32) DEFAULT NULL,
  `rx_bytes` bigint(20) unsigned NOT NULL DEFAULT 0,
  `rx_packets` int(10) unsigned NOT NULL DEFAULT 0,
  `rx_errs` smallint(10) unsigned NOT NULL DEFAULT 0,
  `rx_drop` smallint(10) unsigned NOT NULL DEFAULT 0,
  `tx_bytes` bigint(20) unsigned NOT NULL DEFAULT 0,
  `tx_packets` int(10) unsigned NOT NULL DEFAULT 0,
  `tx_errs` smallint(10) unsigned NOT NULL DEFAULT 0,
  `tx_drop` smallint(10) unsigned NOT NULL DEFAULT 0,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`_interface_stat`),
  KEY `interface` (`interface`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
