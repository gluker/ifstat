-- MySQL dump 10.16  Distrib 10.3.7-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: ifstat
-- ------------------------------------------------------
-- Server version	10.3.7-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `super_option`
--

DROP TABLE IF EXISTS `super_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `super_option` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `super_interface` varchar(32) DEFAULT NULL,
  `interface_table` varchar(32) DEFAULT NULL,
  `interface_name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `super_interface_2` (`super_interface`,`interface_table`,`interface_name`),
  KEY `super_interface` (`super_interface`),
  KEY `interface_table` (`interface_table`),
  KEY `interface_name` (`interface_name`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `super_option`
--

LOCK TABLES `super_option` WRITE;
/*!40000 ALTER TABLE `super_option` DISABLE KEYS */;
INSERT INTO `super_option` VALUES (1,'wan','lat','eth0'),(2,'lan','lat','eth4'),(3,'wan','bkfon','eth0'),(4,'lan','bkfon','eth1'),(5,'wan','cuo','eth0'),(6,'lan','cuo','eth1'),(7,'wan','dvt','bond0'),(8,'lan','dvt','bond1'),(9,'wan','lv','eth0'),(10,'lan','lv','eth1'),(11,'wan','6de','eth0'),(12,'lan','6de','eth1'),(13,'wan','cor','eth0'),(14,'lan','cor','eth1'),(15,'wan','8vdo','eth0'),(16,'lan','8vdo','eth1');
/*!40000 ALTER TABLE `super_option` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-11 17:04:28
