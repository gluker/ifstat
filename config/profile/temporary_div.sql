INSERT INTO
    tmp(
        value,
        interface,
        day_of_week,
        hour_of_day)
SELECT
    SUM({divided}) / NULLIF(SUM({divisor}), 0),
    i.interface,
    WEEKDAY(timestamp) AS day_of_week,
    HOUR(timestamp) AS hour_of_day
FROM
    interface_{hostname} AS i,
    preprofile_{hostname} AS p
WHERE
    i.interface = p.interface AND
    WEEKDAY(timestamp) = p.day_of_week AND
    HOUR(timestamp) = p.hour_of_day AND
    i.{divided} / NULLIF(i.{divisor}, 0) {less_greater} 
        p.{divided}__div__{divisor}
    {exclude_anomaly}
GROUP BY
    p.interface,
    p.day_of_week,
    p.hour_of_day;
