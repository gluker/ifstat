INSERT INTO
    profile_{hostname}(
        interface,
        day_of_week,
        hour_of_day)
SELECT
    interface,
    day_of_week,
    hour_of_day
FROM
    preprofile_{hostname};
