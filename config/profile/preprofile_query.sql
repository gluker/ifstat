INSERT INTO
    preprofile_{hostname}(
        interface,
        day_of_week,
        hour_of_day,
        rx_bytes__avg,
        rx_packets__avg,
        tx_bytes__avg,
        tx_packets__avg,
        rx_tx_drop_errs__avg,
        rx_bytes__div__rx_packets,
        tx_bytes__div__tx_packets,
        tx_bytes__div__rx_bytes,
        tx_packets__div__rx_packets)
SELECT
    i.interface,
    WEEKDAY(timestamp) AS day_of_week,
    HOUR(timestamp) AS hour_of_day,
    ROUND(AVG(rx_bytes)) AS rx_bytes__avg,
    ROUND(AVG(rx_packets)) AS rx_packets__avg,
    ROUND(AVG(tx_bytes)) AS tx_bytes__avg,
    ROUND(AVG(tx_packets)) AS tx_packets__avg,
    AVG(rx_errs + rx_drop + tx_errs + tx_drop) AS rx_tx_drop_errs__avg,
    SUM(rx_bytes) / NULLIF(SUM(rx_packets), 0) AS rx_bytes__div__rx_packets,
    SUM(tx_bytes) / NULLIF(SUM(tx_packets), 0) AS tx_bytes__div__tx_packets,
    SUM(tx_bytes) / NULLIF(SUM(rx_bytes), 0) AS tx_bytes__div__rx_bytes,
    SUM(tx_packets) / NULLIF(SUM(rx_packets), 0) AS tx_packets__div__rx_packets
FROM
    interface_{hostname} AS i
WHERE
    1 = 1 {exclude_anomaly}
GROUP BY
    i.interface,
    day_of_week,
    hour_of_day;
