INSERT INTO
    tmp(
        value,
        interface,
        day_of_week,
        hour_of_day)
SELECT
    AVG(rx_errs + rx_drop + tx_errs + tx_drop),
    p.interface,
    p.day_of_week,
    p.hour_of_day
FROM
    interface_{hostname} AS i,
    preprofile_{hostname} AS p
WHERE
    rx_errs + rx_drop + tx_errs + tx_drop {less_greater}
        p.rx_tx_drop_errs__avg AND
    i.interface = p.interface AND
    WEEKDAY(timestamp) = p.day_of_week AND
    HOUR(timestamp) = p.hour_of_day
    {exclude_anomaly}
GROUP BY
    p.interface,
    p.day_of_week,
    p.hour_of_day;
