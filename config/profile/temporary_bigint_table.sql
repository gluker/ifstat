CREATE TABLE `tmp` (
  `value` bigint unsigned DEFAULT NULL,
  `interface` varchar(32) CHARACTER SET ascii DEFAULT NULL,
  `day_of_week` tinyint unsigned DEFAULT NULL,
  `hour_of_day` tinyint unsigned DEFAULT NULL,
  PRIMARY KEY(`interface`,`day_of_week`,`hour_of_day`),
  KEY `value` (`value`)
) ENGINE=MEMORY;
