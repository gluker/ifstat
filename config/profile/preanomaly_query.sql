INSERT INTO
    preanomaly_{hostname}(
        interface,
        timestamp)
SELECT
    t.iface,
    t.timestamp
FROM (
    SELECT
        i.interface AS iface,
        i.timestamp,
        p.*
    FROM
        interface_{hostname} AS i
    INNER JOIN
        profile_{hostname} AS p
    ON
        i.interface = p.interface AND
        WEEKDAY(i.timestamp) = p.day_of_week AND
        HOUR(i.timestamp) = p.hour_of_day
    GROUP BY
        i.interface,
        i.timestamp
    HAVING
        AVG(i.rx_bytes) / NULLIF(p.rx_bytes__avg_low, 0) > {delta} OR
        p.rx_bytes__avg_high / NULLIF(AVG(i.rx_bytes), 0) > {delta} OR
        AVG(rx_packets) / NULLIF(p.rx_packets__avg_low, 0) > {delta} OR
        p.rx_packets__avg_high / NULLIF(AVG(rx_packets), 0) > {delta} OR
        AVG(tx_bytes) / NULLIF(p.tx_bytes__avg_low, 0) > {delta} OR
        p.tx_bytes__avg_high / NULLIF(AVG(tx_bytes), 0) > {delta} OR
        AVG(tx_packets) / NULLIF(p.tx_packets__avg_low, 0) > {delta} OR
        p.tx_packets__avg_high / NULLIF(AVG(tx_packets), 0) > {delta} OR
        AVG(rx_errs + rx_drop + tx_errs + tx_drop) /
            NULLIF(p.rx_tx_drop_errs__avg_low, 0) > {delta} OR
        p.rx_tx_drop_errs__avg_high /
            NULLIF(AVG(rx_errs + rx_drop + tx_errs + tx_drop), 0) > {delta} OR
        (SUM(rx_bytes) / NULLIF(SUM(rx_packets), 0)) /
            NULLIF(p.rx_bytes__div__rx_packets__low, 0) > {delta} OR
        p.rx_bytes__div__rx_packets__high /
            NULLIF((SUM(rx_bytes) / NULLIF(SUM(rx_packets), 0)), 0) > {delta} OR
        (SUM(tx_bytes) / NULLIF(SUM(tx_packets), 0)) /
            NULLIF(p.tx_bytes__div__tx_packets__low, 0) > {delta} OR
        p.tx_bytes__div__tx_packets__high /
            NULLIF((SUM(tx_bytes) / NULLIF(SUM(tx_packets), 0)), 0) > {delta} OR
        (SUM(tx_bytes) / NULLIF(SUM(rx_bytes), 0)) /
            NULLIF(p.tx_bytes__div__rx_bytes__low, 0) > {delta} OR
        p.tx_bytes__div__rx_bytes__high /
            NULLIF((SUM(tx_bytes) / NULLIF(SUM(rx_bytes), 0)), 0) > {delta} OR
        (SUM(tx_packets) / NULLIF(SUM(rx_packets), 0)) /
            NULLIF(p.tx_packets__div__rx_packets__low, 0) > {delta} OR
        p.tx_packets__div__rx_packets__high /
            NULLIF((SUM(tx_packets) / NULLIF(SUM(rx_packets), 0)), 0) > {delta}
    ) AS t

