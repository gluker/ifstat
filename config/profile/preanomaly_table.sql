CREATE TABLE `preanomaly_{hostname}` (
  `interface` varchar(32) CHARACTER SET ascii DEFAULT NULL,
  `timestamp` timestamp DEFAULT 0,
  PRIMARY KEY (`interface`,`timestamp`),
  KEY `interface` (`interface`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;
