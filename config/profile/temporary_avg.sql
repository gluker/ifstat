INSERT INTO
    tmp(
        value,
        interface,
        day_of_week,
        hour_of_day)
SELECT
    ROUND(AVG({column})),
    p.interface,
    p.day_of_week,
    p.hour_of_day
FROM
    interface_{hostname} AS i,
    preprofile_{hostname} AS p
WHERE
    i.{column} {less_greater} p.{column}__avg AND
    i.interface = p.interface AND
    WEEKDAY(timestamp) = p.day_of_week AND
    HOUR(timestamp) = p.hour_of_day 
    {exclude_anomaly}
GROUP BY
    p.interface,
    p.day_of_week,
    p.hour_of_day;
