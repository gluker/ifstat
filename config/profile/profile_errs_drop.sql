UPDATE
    profile_{hostname} p
INNER JOIN
    tmp t
ON
    p.interface = t.interface AND
    p.day_of_week = t.day_of_week AND
    p.hour_of_day = t.hour_of_day
SET
    p.rx_tx_drop_errs__avg_{low_high} = t.value;
