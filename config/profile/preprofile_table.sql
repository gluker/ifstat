CREATE TABLE `preprofile_{hostname}` (
  `interface` varchar(32) CHARACTER SET ascii DEFAULT NULL,
  `day_of_week` tinyint unsigned DEFAULT NULL,
  `hour_of_day` tinyint unsigned DEFAULT NULL,
  `rx_bytes__avg` bigint unsigned DEFAULT NULL,
  `rx_packets__avg` bigint unsigned DEFAULT NULL,
  `tx_bytes__avg` bigint unsigned DEFAULT NULL,
  `tx_packets__avg` bigint unsigned DEFAULT NULL,
  `rx_tx_drop_errs__avg` double unsigned NOT NULL DEFAULT '0',
  `rx_bytes__div__rx_packets` double unsigned DEFAULT NULL,
  `tx_bytes__div__tx_packets` double unsigned DEFAULT NULL,
  `tx_bytes__div__rx_bytes` double unsigned DEFAULT NULL,
  `tx_packets__div__rx_packets` double unsigned DEFAULT NULL,
  PRIMARY KEY (`interface`,`day_of_week`,`hour_of_day`),
  KEY (`interface`),
  KEY (`day_of_week`),
  KEY (`hour_of_day`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;
