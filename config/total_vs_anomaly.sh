#!/bin/bash

m='mysql -e'
calc () { 
    eval "perl -e 'print ($@);'" && echo
}

for i in `$m 'show tables' | grep interface_ | sed 's|interface_||g'` ; do 
    total=`$m "select count(*) from interface_${i}" | grep -v count`
    anomalies=`$m "select count(*) from preanomaly_${i}" | grep -v count`
    echo "$i: $total/$anomalies; `calc $total/$anomalies`"
done
