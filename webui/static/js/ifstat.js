$(function() {
    var overviewArrowLeft = overviewArrowRight = false;
    var weekTime = 7 * 24 * 60 * 60 * 1000;
    var tzOffset = new Date().getTimezoneOffset() * 60 * 1000;
    var monthNames = ['янв', 'февр', 'март', 'апр', 'май', 'июнь', 
                      'июль', 'авг', 'сент', 'окт', 'нояб', 'дек'];
    var optionsPlot = {
        canvas: true,
        series: {
            shadowSize: 0
        },
        lines: {
            show: true,
            fill: true
        },
        points: {
            show: false
        },
        xaxis: {
            show: true,
            mode: "time",
            monthNames: monthNames
        },
        yaxis: {
            axisLabel: 'Mbit/s',
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 18,
        },
        grid: {
            aclickable: true,
            show: true,
            hoverable: true,
            borderColor: "#ccc",
            backgroundColor: {
                colors: ["#ffe", "#fff"]
            }
        },
        legend: {
            backgroundOpacity: 0.5,
            position: "nw",
            noColumns: 0
        },
        selection: {
            mode: "x",
        }
    };

    var optionsOverview = {
        series: {
            lines: {
                show: true,
                lineWidth: 1,
                fill: true
            },
            shadowSize: 3
        },
        xaxis: {
            mode: "time",
            timeformat: "%b %d",
            monthNames: monthNames
       },
        yaxis: {
            ticks: [],
            min: 0,
            autoscaleMargin: 0
        },
        selection: {
            mode: "x",
            minSize: 0
        }
    };


    var prevPoint = prevLabel = null;

    $.fn.UseTooltip = function () {
        $(this).bind("plothover", function (event, pos, item) {
            if (item) {
                if ((prevLabel != item.series.label) ||
                            (prevPoint != item.dataIndex)) {
                    prevPoint = item.dataIndex;
                    prevLabel = item.series.label;
                    $("#tooltip").remove();

                    var date = item.datapoint[0];
                    var label = item.series.label.split('_');
                    var date = ('0' + new Date(date).getUTCHours()).slice(-2) +
                        ":" + ('0' + new Date(date).getUTCMinutes()).slice(-2);

                    showTooltip(item.pageX, item.pageY, item.series.color,
                        "<strong>" + date + ", " + label[0].toUpperCase() +
                        ":<br>" + item.datapoint[1].toFixed(2) + "</strong> " + 
                        label[1]);
                }
            } else {
                $("#tooltip").remove();
                prevPoint = null;
            }
        });
    };


    function showTooltip(x, y, color, contents) {
        $('<div id="tooltip">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y - 55,
            left: x - 40,
            border: '2px solid ' + color,
            padding: '3px',
            'font-size': '12px',
            'border-radius': '5px',
            'background-color': '#fff',
            'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            opacity: 0.8
        }).appendTo("body").fadeIn(200);
    }



    var overview = null;
    var plot = { bytes: null, packets: null };
    var timeoutOverview = timeoutPlot = null;
    var timeStepPlot = ifstatInterval * 1000;
    var overviewTimeEnd = plotTimeStart = plotTimeEnd = plotTimeLatest = 0;
    var overviewTimeStart = new Date().getTime() - tzOffset - weekTime;  


    function getURLPath() {
        url = window.location.href.split("#");
        if (url[1]) {
            times = url[1].split("/");

            time = times[0].replace(/-/g, "/").replace(/_/g, " ");
            plotTimeStart = new Date(time).getTime() - tzOffset;

            time = times[1].replace(/-/g, "/").replace(/_/g, " ");
            plotTimeEnd = new Date(time).getTime() - tzOffset;

            plotTimeLatest = new Date().getTime() - tzOffset;

            if (plotTimeLatest - plotTimeStart > weekTime) {
                overviewTimeStart = plotTimeStart - weekTime / 7 * 3;
                overviewTimeEnd   = plotTimeStart + weekTime / 7 * 4;
                if (overviewTimeEnd < plotTimeEnd)
                    overviewTimeEnd = plotTimeEnd + weekTime / 7 * 4;
            } else {
                overviewTimeStart = plotTimeLatest - weekTime;
                overviewTimeEnd   = 0;
            }
        }
    }


    function setURLPath() {
        url = window.location.href.split("#");
        location = url[0] + "#" + 
                    timeJS2Human(plotTimeStart).replace(/ /g, "_") + "/" + 
                    timeJS2Human(plotTimeEnd).replace(/ /g, "_");
    }


    function clearURLPath() {
        url = window.location.href.split("#");
        location = url[0] + "#";
    }



    function leadingZero(value) {
        return (parseInt(value) >= 10) ? value : ('0' + value);
    }


    function timeJS2Human(timestamp) {
        stamp = new Date(timestamp);
        return( stamp.getUTCFullYear() + "-" +
                leadingZero(stamp.getUTCMonth() + 1) + '-' +
                leadingZero(stamp.getUTCDate()) + " " +
                leadingZero(stamp.getUTCHours()) + ":" +
                leadingZero(stamp.getUTCMinutes()) + ":" +
                leadingZero(stamp.getUTCSeconds()));
    }



    function fetchDataOverview() {
        function onDataReceived(series) {
            overview = $.plot("#overview", series.bytes, optionsOverview);
            ranges = { xaxis: { from: plotTimeStart, to: plotTimeEnd } }
            overview.setSelection(ranges, true);
        }
        
        if (overviewTimeStart > oldestTimestamp) {
            overviewArrowLeft = true;
            $("#arrow-left").show();
        } else {
            overviewArrowLeft = false;
            $("#arrow-left").hide();
        }

        if (overviewTimeEnd) {
            overviewArrowRight = true;
            $("#arrow-right").show();
        } else {
            overviewArrowRight = false;
            $("#arrow-right").hide();
        }

        var requestURL = "/stat/" + currentInterface + "/";
        if (overviewTimeStart)
            requestURL += timeJS2Human(overviewTimeStart) + "/";
        else
            requestURL += timeJS2Human(latestTimestamp - weekTime) + "/";
        if (overviewTimeEnd)
            requestURL += timeJS2Human(overviewTimeEnd);
        else
            requestURL += "padding";
        requestURL += "/overview";

        clearTimeout(timeoutOverview);
        if (!overviewTimeEnd)
            timeoutOverview = setTimeout(fetchDataOverview, timeStepPlot*13/3);

        $.ajax({
            url: requestURL,
            type: "GET",
            dataType: "json",
            success: onDataReceived
        });
    }


    function fetchDataPlot() {
        function onDataReceived(series) {
            var dataTime = series.bytes[0].data;

            if (dataTime[0] === undefined) {
                plotTimeStart = latestTimestamp - 3 * 60 * 60 * 1000;
                fetchDataPlot();
                return;
            }

            if (!plotTimeStart)
                plotTimeStart = dataTime[0][0];

            plotTimeEnd = dataTime[dataTime.length - 1][0];

            if (!plotTimeLatest || plotTimeLatest < plotTimeEnd)
                plotTimeLatest = plotTimeEnd;

            var timeDiff = ((plotTimeEnd) ? plotTimeEnd : 
                        new Date().getTime() - tzOffset) - plotTimeStart;
            if (timeDiff > 2 * 24 * 60 * 60 * 1000 &&
                    timeDiff < 6.5 * 24 * 60 * 60 * 1000)
                optionsPlot.xaxis.timeformat = "%b %d<br>%H:%M";
            else
                optionsPlot.xaxis.timeformat = null;

            if (overview != null) {
                ranges = { xaxis: { from: plotTimeStart, to: plotTimeEnd } }
                overview.setSelection(ranges, true);
            }

            optionsPlot.yaxis.axisLabel = 'Mbit/s';
            plot.bytes = $.plot("#bytes", series.bytes, optionsPlot);
            $("#bytes").UseTooltip();

            optionsPlot.yaxis.axisLabel = 'Kpacket/s';
            plot.packets = $.plot("#packets", series.packets, optionsPlot);
            $("#packets").UseTooltip();
        }

        var requestURL = "/stat/" + currentInterface;
        if (plotTimeStart) {
            if (plotTimeEnd && plotTimeLatest - timeStepPlot*10 > plotTimeEnd) {
                requestURL     += "/" + timeJS2Human(plotTimeStart) + 
                                  "/" + timeJS2Human(plotTimeEnd);
                setURLPath();
            } else {
                plotTimeEnd     = 0;
                plotTimeStart  += timeStepPlot;
                requestURL     += "/" + timeJS2Human(plotTimeStart);
                clearURLPath();
            }
        }

        clearTimeout(timeoutPlot);
        if (!plotTimeEnd || plotTimeLatest - timeStepPlot * 10 < plotTimeEnd)
                timeoutPlot = setTimeout(fetchDataPlot, timeStepPlot);

        $.ajax({
            url: requestURL,
            type: "GET",
            dataType: "json",
            success: onDataReceived
        });
    }



    $("#bytes").bind("plotselected", function (event, ranges) {
        $.each(plot.bytes.getXAxes(), function(_, axis) {
            var opts        = axis.options;
            opts.min        = ranges.xaxis.from;
            opts.max        = ranges.xaxis.to;
            plotTimeStart   = opts.min;
            plotTimeEnd     = opts.max;
        });

        fetchDataPlot();

        plot.packets.setSelection(ranges, true);
        overview.setSelection(ranges, true);
    });


    $("#packets").bind("plotselected", function (event, ranges) {
        $.each(plot.packets.getXAxes(), function(_, axis) {
            var opts        = axis.options;
            opts.min        = ranges.xaxis.from;
            opts.max        = ranges.xaxis.to;
            plotTimeStart   = opts.min;
            plotTimeEnd     = opts.max;
        });

        fetchDataPlot();

        plot.bytes.setSelection(ranges, true);
        overview.setSelection(ranges, true);
    });


    $("#overview").bind("plotselected", function (event, ranges) {
        plot.bytes.setSelection(ranges);
    });



    function rebuildOverviewPlot() {
        plotTimeStart   = overviewTimeStart;
        plotTimeEnd     = overviewTimeEnd;
        fetchDataOverview();
        fetchDataPlot();
    }


    $("#arrow-left").bind("click", function(event) {
        if ($(this).is(":visible")) { 
            if (overviewTimeStart - weekTime > oldestTimestamp) {
                overviewTimeEnd    = overviewTimeStart;
                overviewTimeStart -= weekTime;
            } else {
                overviewTimeEnd    = oldestTimestamp + weekTime;
                overviewTimeStart  = oldestTimestamp;
            }
            rebuildOverviewPlot();
        }
    });


    $("#arrow-right").bind("click", function(event) {
        if ($(this).is(":visible")) { 
            if (overviewTimeEnd + weekTime > new Date().getTime() - tzOffset) {
                overviewTimeEnd    = 0;
                overviewTimeStart  = new Date().getTime() - tzOffset - weekTime;
            } else {
                overviewTimeEnd   += weekTime;
                overviewTimeStart += weekTime;
            }
            rebuildOverviewPlot();
        }
    });


    $("#arrow-left, #arrow-right").bind("hover", function(event) {
        if ($(this).is(":visible")) 
            document.body.style.cursor = 'pointer';
    });


    $("#arrow-left, #arrow-right").bind("mouseleave", function(event) {
        document.body.style.cursor = 'default';
    });



    $("#overview").resize(function() {
        fetchDataOverview();
    });



    getURLPath();

    fetchDataOverview();

    fetchDataPlot();
});

